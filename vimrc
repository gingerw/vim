set nocompatible
filetype off

" For Pathogen to work when loading:
runtime bundle/vim-pathogen/autoload/pathogen.vim
call pathogen#infect()
call pathogen#helptags()

filetype indent plugin on
filetype plugin on

" For CommandT to work well:
nmap <leader>o <Esc>:CommandT<CR>
nmap <leader>O <Esc>:CommandTFlush<CR>
nmap <leader>m <Esc>:CommandTBuffer<CR>

" For Puppet => assign:
nmap <leader>a> :Tabularize /=><CR>
vmap <leader>a> :Tabularize /=><CR>

" For ClosTag to work well:
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS 
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags 
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS 
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete 
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags 
"autocmd BufRead,BufNewFile   *.pp setfiletype puppet

" For Tagbar :
let g:tagbar_usearrows = 1
nnoremap <leader>t :TagbarToggle<CR>

"For powerline:
set laststatus=2 
set t_Co=256

" Other configs:
syntax on
set nu
set ic 
set hlsearch
set tabstop=8 expandtab shiftwidth=4 softtabstop=4
" A better way to deal with indent problem, use <F12> to toggle paste mode on/off
set autoindent
set pastetoggle=<F12>

"neocomplcache
"<CR>: close popup and save indent.
inoremap <expr><CR>  neocomplcache#smart_close_popup() . "\<CR>"
"<TAB>: completion. NO USE with snipmate
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>" 
"<C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplcache#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplcache#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplcache#close_popup()
inoremap <expr><C-e>  neocomplcache#cancel_popup()
"inoremap <expr><Enter>  pumvisible() ? neocomplcache#close_popup()."\<C-n>" : "\<Enter>"
inoremap <expr><Enter>  pumvisible() ? "\<C-Y>" : "\<Enter>"

let g:neocomplcache_enable_at_startup = 1 
" Use smartcase.
let g:neocomplcache_enable_smart_case = 1
" Use camel case completion.
let g:neocomplcache_enable_camel_case_completion = 1
" Use underscore completion.
let g:neocomplcache_enable_underbar_completion = 1
" Sets minimum char length of syntax keyword.
let g:neocomplcache_min_syntax_length = 4
"Enable omni completion. 

"nerd tree
nmap ,ne <ESC>:NERDTreeToggle<CR>

"syntastic
let g:syntastic_enable_signs=1
let g:syntastic_quiet_messages={'level': 'warnings'} 
let g:syntastic_auto_loc_list=1
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"For Richard's vim-colors-solarized 
set background=dark

"Json auto formatting use python json.tool module
nmap <Leader>jf <ESC>:%!python -m json.tool <CR>
"Disable concealing of double quotes in json file
let g:vim_json_syntax_conceal = 0

"SQL Formatting:
vmap <silent>sf        <Plug>SQLU_Formatter<CR>
nmap <silent>scl       <Plug>SQLU_CreateColumnList<CR>
nmap <silent>scd       <Plug>SQLU_GetColumnDef<CR>
nmap <silent>scdt      <Plug>SQLU_GetColumnDataType<CR>
nmap <silent>scp       <Plug>SQLU_CreateProcedure<CR>

"Some personal shortcuts:
nmap ,pz <ESC>:vsplit ~/.vimrc
set nobackup
nmap ,tpy <ESC>:w<CR><ESC>:!/usr/bin/python % <CR>
nmap ,tr <ESC>:w<CR><ESC>:!/usr/bin/env ruby % <CR>
nmap ,vp <ESC>:wa<CR><ESC>:!/usr/bin/rake test<CR>
nmap ,vs <ESC>:!vagrant status<CR>
nmap ,app <ESC>:w<CR><ESC>:!/usr/bin/puppet apply % <CR>

