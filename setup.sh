#!/bin/bash 
# -*- encoding: utf-8 -*-
# Copyright (C) 2013 Edanz System Team
###
# Author : yexing <yexingok@gmail.com>;
# Desc   : To setup my vim environment on server
# Created: 2013-02-18
# Updated: Ver 0.1 the first version.
###

PWD=`pwd`

if [ ! -e ~/.vim ] ; then
    ln -s ${PWD} ~/.vim
fi

if [ ! -e ~/.vimrc ] ; then
    ln -s ${PWD}/vimrc ~/.vimrc
fi

for file in `ls -a ${PWD}/bash` ; do
    if [ ! -e ~/${file} ] ; then
        ln -s ${PWD}/bash/${file} ~/${file}
    fi
done

#Initial Git submodule
git submodule init
git submodule update
git submodule foreach git pull origin master
